<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\UsersQuestionController;
use Illuminate\Support\Facades\Route;

Route::get('/', [AuthController::class, 'showLogin'])->name('login');
Route::get('login', [AuthController::class, 'showLogin'])->name('login');
Route::post('login', [AuthController::class, 'login']);

Route::middleware(['auth'])->group(function () {
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('dashboard', DashboardController::class);
    Route::prefix('questions')->group(function () {
        Route::get('list', [QuestionController::class, 'index'])->name('questions.list');
        Route::get('create', [QuestionController::class, 'create'])->name('questions.create');
        Route::post('store', [QuestionController::class, 'store'])->name('questions.store');
    });
    Route::prefix('users/questions')->group(function () {
        Route::get('/', [UsersQuestionController::class, 'index'])->name('user.questions.index');
        Route::get('/list', [UsersQuestionController::class, 'list'])->name('user.questions.list');
        Route::post('/store', [UsersQuestionController::class, 'store'])->name('user.questions.store');
        Route::get('/view/{id}', [UsersQuestionController::class, 'view'])->name('user.questions.view');
    });
});

