<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Theme Made By www.w3schools.com - No Copyright -->
    <title>Questions List</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        p {
            font-size: 16px;
        }

        .navbar {
            padding-top: 15px;
            padding-bottom: 15px;
            border: 0;
            border-radius: 0;
            margin-bottom: 0;
            font-size: 12px;
            letter-spacing: 5px;
        }

        .navbar-nav li a:hover {
            color: #1abc9c !important;
        }
    </style>
</head>
<body>

<!-- Navbar -->
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/dashboard">Me</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                @if (\Illuminate\Support\Facades\Auth::user()->is_admin === 1)
                    <li><a href="/users/questions/list">User Questions</a></li>
                    <li><a href="/questions/list">Questions</a></li>
                @endif
                <li><a href="/logout">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container" style="margin-top: 40px">
    <div class="row">
        <h2>User Questions Answer</h2>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Title</th>
                <th>Type</th>
                <th>Answer</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($usersQuestionsDetails as $usersQuestionsDetail)
                <tr>
                    <td>{{ $usersQuestionsDetail->question->title }}</td>
                    <td>{{ $usersQuestionsDetail->question->type }}</td>
                    @if ($usersQuestionsDetail->question->type === \App\Models\Questions::PROGRESS)
                        <td> - </td>
                        <td>
                        @foreach (json_decode($usersQuestionsDetail->description, true) as $key => $answer)
                            {{ $answer . ' => ' . $key  }} <br/>
                        @endforeach
                        </td>
                    @else
                        <td>{{ $usersQuestionsDetail->answer }}</td>
                        <td>{{ $usersQuestionsDetail->description }}</td>
                    @endif

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
