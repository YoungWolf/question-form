<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Theme Made By www.w3schools.com - No Copyright -->
    <title>Questions List</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        p {
            font-size: 16px;
        }

        .navbar {
            padding-top: 15px;
            padding-bottom: 15px;
            border: 0;
            border-radius: 0;
            margin-bottom: 0;
            font-size: 12px;
            letter-spacing: 5px;
        }

        .navbar-nav li a:hover {
            color: #1abc9c !important;
        }
    </style>
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/dashboard">Me</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                @if (\Illuminate\Support\Facades\Auth::user()->is_admin === 1)
                    <li><a href="/users/questions/list">User Questions</a></li>
                    <li><a href="/questions/list">Questions</a></li>
                @endif
                <li><a href="/logout">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <h2>Questions Form</h2>
    {{ Form::open(array('url' => 'questions/store')) }}
    <div class="row">
        <div class="form-group col-sm-12">
            <label for="email">Question Title:</label>
            <input type="text" class="form-control" id="title" placeholder="Question Title" name="title">
        </div>
        <div class="form-group col-sm-6">
            <label for="pwd">Question Type:</label>
            <select class="form-control" id="type" name="type">
                <option>Radio</option>
                <option>Progress</option>
                <option>Descriptive</option>
                <option>Multiple choice</option>
            </select>
        </div>
        <div class="form-group col-sm-6">
            <label for="pwd">Question Page:</label>
            <select class="form-control" id="page" name="page">
                <option>1</option>
                <option>2</option>
                <option>3</option>
            </select>
        </div>
        <div class="multiple_choice_answer">
            <div class="form-group col-sm-6">
                <label for="email">First Answer:</label>
                <input type="text" class="form-control" id="first-answer" placeholder="First Answer " name="options[]">
            </div>
            <div class="form-group col-sm-6">
                <label for="email">Second Answer:</label>
                <input type="text" class="form-control" id="second-answer" placeholder="Second Answer " name="options[]">
            </div>
            <div class="form-group col-sm-6">
                <label for="email">Third Answer:</label>
                <input type="text" class="form-control" id="third-answer" placeholder="Third Answer " name="options[]">
            </div>
            <div class="form-group col-sm-6">
                <label for="email">Fourth Answer:</label>
                <input type="text" class="form-control" id="fourth-answer" placeholder="Fourth Answer " name="options[]">
            </div>
        </div>
        <div class="multiple_progress_answer">
            <div class="form-group col-sm-6">
                <label for="email">First Progress:</label>
                <input type="text" class="form-control" id="first-answer" placeholder="First Answer " name="options[]">
            </div>
            <div class="form-group col-sm-6">
                <label for="email">Second Progress:</label>
                <input type="text" class="form-control" id="second-answer" placeholder="Second Answer " name="options[]">
            </div>
            <div class="form-group col-sm-6">
                <label for="email">Third Progress:</label>
                <input type="text" class="form-control" id="third-answer" placeholder="Third Answer " name="options[]">
            </div>
            <div class="form-group col-sm-6">
                <label for="email">Fourth Progress:</label>
                <input type="text" class="form-control" id="fourth-answer" placeholder="Fourth Answer " name="options[]">
            </div>
            <div class="form-group col-sm-6">
                <label for="email">Fifth Progress:</label>
                <input type="text" class="form-control" id="fifth-answer" placeholder="Fifth Answer " name="options[]">
            </div>
            <div class="form-group col-sm-6">
                <label for="email">Sixth Progress:</label>
                <input type="text" class="form-control" id="sixth-answer" placeholder="Sixth Answer " name="options[]">
            </div>
        </div>
    </div>
    <div class="row text-center">
        <button type="submit" class="btn btn-success">Submit</button>
    </div>
    {{ Form::close() }}
</div>
</body>
</html>

<script>
    $(document).ready(function () {
        $('.multiple_choice_answer').hide();
        $('.multiple_progress_answer').hide();
        $("#type").change(function () {
            let questionType = $(this).val();
            if (questionType == 'Multiple choice') {
                $('.multiple_choice_answer').show(1000);
            } else {
                $('.multiple_choice_answer').hide(1000);
            }

            if (questionType == 'Progress') {
                $('.multiple_progress_answer').show(1000);
            } else {
                $('.multiple_progress_answer').hide(1000);
            }
        });
    });
</script>
