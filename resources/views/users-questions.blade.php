<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Theme Made By www.w3schools.com - No Copyright -->
    <title>Questions List</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        body {
            direction: rtl;
        }

        p {
            font-size: 16px;
        }

        .navbar {
            padding-top: 15px;
            padding-bottom: 15px;
            border: 0;
            border-radius: 0;
            margin-bottom: 0;
            font-size: 12px;
            letter-spacing: 5px;
        }

        .navbar-nav li a:hover {
            color: #1abc9c !important;
        }
    </style>
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/dashboard">Me</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                @if (\Illuminate\Support\Facades\Auth::user()->is_admin === 1)
                    <li><a href="/users/questions/list">User Questions</a></li>
                    <li><a href="/questions/list">Questions</a></li>
                @endif
                <li><a href="/logout">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <form action="/users/questions/store" method="post">
        <div class="alert alert-danger" role="alert" style="margin-top: 40px">
            توجه : پر کردن تمامی موارد الزامی می باشد.
        </div>
        <div class="row pageOne">
            <div class="row">
                <div class="text-center" style="margin-top: 25px">
                    <button type="button" class="btn btn-success">صفحه اول</button>
                </div>
                @foreach ($pageOneQuestions as $pageOneQuestion)
                    @csrf
                    <h4> سوال : {{$pageOneQuestion['title']}}</h4>
                    <div style="display: none">
                        <input type="text" name="answer[{{$pageOneQuestion['id']}}][type]"
                               value="{{$pageOneQuestion['type']}}" required>
                        <input type="text" name="answer[{{$pageOneQuestion['id']}}][question_id]"
                               value="{{$pageOneQuestion['id']}}" required>
                    </div>
                    @if ($pageOneQuestion['type'] === \App\Models\Questions::RADIO)
                        <input type="radio" class="radio-question" id="{{$pageOneQuestion['id']}}"
                               name="answer[{{$pageOneQuestion['id']}}][answer]" value="disagree" required>
                        <label for="age1">مخالفم</label><br>
                        <input type="radio" class="radio-question" id="{{$pageOneQuestion['id']}}"
                               name="answer[{{$pageOneQuestion['id']}}][answer]" value="agree" required>
                        <label for="age2">موافقم</label><br>
                        <div class="questions-descriptions description_{{$pageOneQuestion['id']}}">
                        <textarea id="answer" class="form-control" style="width: 100%; height: 100px"
                                  name="answer[{{$pageOneQuestion['id']}}][description]"></textarea>
                        </div>
                    @elseif ($pageOneQuestion['type'] === \App\Models\Questions::PROGRESS)
                        @foreach (json_decode($pageOneQuestion['options'], true) as $answer)
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="range" class="form-control-range"
                                           name="answer[{{$pageOneQuestion['id']}}][answer][]"
                                           min="0" max="10" id="{{$pageOneQuestion['id']}}"
                                           value="0"
                                           oninput="this.nextElementSibling.value = this.value"
                                    >
                                    <output>0</output>
                                </div>
                                <div class="col-sm-6">
                                    <input type="checkbox" class="radio-question" id="{{$pageOneQuestion['id']}}"
                                           name="answer[{{$pageOneQuestion['id']}}][description][]" value="{{$answer}}">
                                    <label for="age2">{{$answer}}</label><br>
                                </div>
                            </div>
                        @endforeach

                    @elseif ($pageOneQuestion['type'] === \App\Models\Questions::DESCRIPTION)
                        <textarea id="{{$pageOneQuestion['id']}}" class="form-control"
                                  style="width: 100%; height: 100px"
                                  name="answer[{{$pageOneQuestion['id']}}][answer]" required></textarea>
                    @elseif ($pageOneQuestion['type'] === \App\Models\Questions::MULTIPLE_CHOICE)
                        @foreach (json_decode($pageOneQuestion['options'], true) as $answer)
                            <input type="radio" id="{{$pageOneQuestion['id']}}"
                                   name="answer[{{$pageOneQuestion['id']}}][answer]"
                                   value="{{$answer}}" required>
                            <label for="{{$pageOneQuestion['id']}}">{{$answer}}</label><br>
                        @endforeach
                    @endif
                    <hr>
                @endforeach
            </div>
            <div class="row m-t-4">
                <button type="button" class="btn btn-success second-step">صفحه بعد</button>
            </div>
        </div>
        <div class="row pageTwo">
            <div class="row">
                <div class="text-center" style="margin-top: 25px">
                    <button type="button" class="btn btn-success">صفحه دوم</button>
                </div>
                @foreach ($pageTwoQuestions as $pageTwoQuestion)
                    <h4> سوال : {{$pageTwoQuestion['title']}}</h4>
                    <div style="display: none">
                        <input type="text" name="answer[{{$pageTwoQuestion['id']}}][type]"
                               value="{{$pageTwoQuestion['type']}}" required>
                        <input type="text" name="answer[{{$pageTwoQuestion['id']}}][question_id]"
                               value="{{$pageTwoQuestion['id']}}" required>
                    </div>
                    @if ($pageTwoQuestion['type'] === \App\Models\Questions::RADIO)
                        <input type="radio" class="radio-question" id="{{$pageTwoQuestion['id']}}"
                               name="answer[{{$pageTwoQuestion['id']}}][answer]" value="disagree" required>
                        <label for="age1">مخالفم</label><br>
                        <input type="radio" class="radio-question" id="{{$pageTwoQuestion['id']}}"
                               name="answer[{{$pageTwoQuestion['id']}}][answer]" value="agree" required>
                        <label for="age2">موافقم</label><br>
                        <div class="questions-descriptions description_{{$pageTwoQuestion['id']}}">
                        <textarea id="answer" class="form-control" style="width: 100%; height: 100px"
                                  name="answer[{{$pageTwoQuestion['id']}}][description]"></textarea>
                        </div>
                    @elseif ($pageTwoQuestion['type'] === \App\Models\Questions::PROGRESS)
                        @foreach (json_decode($pageTwoQuestion['options'], true) as $answer)
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="range" class="form-control-range"
                                           name="answer[{{$pageTwoQuestion['id']}}][answer][]"
                                           min="0" max="10" id="{{$pageTwoQuestion['id']}}"
                                           value="0"
                                           oninput="this.nextElementSibling.value = this.value"
                                    >
                                    <output>0</output>
                                </div>
                                <div class="col-sm-6">
                                    <input type="checkbox" class="radio-question" id="{{$pageTwoQuestion['id']}}"
                                           name="answer[{{$pageTwoQuestion['id']}}][description][]" value="{{$answer}}">
                                    <label for="age2">{{$answer}}</label><br>
                                </div>
                            </div>
                        @endforeach
                    @elseif ($pageTwoQuestion['type'] === \App\Models\Questions::DESCRIPTION)
                        <textarea id="{{$pageTwoQuestion['id']}}" class="form-control"
                                  style="width: 100%; height: 100px" required></textarea>
                    @elseif ($pageTwoQuestion['type'] === \App\Models\Questions::MULTIPLE_CHOICE)
                        @foreach (json_decode($pageTwoQuestion['options'], true) as $answer)
                            <input type="radio" id="{{$pageTwoQuestion['id']}}"
                                   name="answer[{{$pageTwoQuestion['id']}}][answer]"
                                   value="{{$answer}}" required>
                            <label for="{{$pageTwoQuestion['id']}}">{{$answer}}</label><br>
                        @endforeach
                    @endif
                    <hr>
                @endforeach
            </div>
            <div class="col-sm-6">
                <button type="button" class="btn btn-success third-step">صفحه بعد</button>
            </div>
            <div class="col-sm-6">
                <button type="button" class="btn btn-success previous-second-step">صفحه قبل</button>
            </div>
        </div>
        <div class="row pageThree">
            <div class="text-center" style="margin-top: 25px">
                <button type="button" class="btn btn-success">صفحه سوم</button>
            </div>
            @foreach ($pageThreeQuestions as $pageThreeQuestion)
                <h4> سوال : {{$pageThreeQuestion['title']}}</h4>
                <div style="display: none">
                    <input type="text" name="answer[{{$pageThreeQuestion['id']}}][type]"
                           value="{{$pageThreeQuestion['type']}}">
                    <input type="text" name="answer[{{$pageThreeQuestion['id']}}][question_id]"
                           value="{{$pageThreeQuestion['id']}}">
                </div>
                @if ($pageThreeQuestion['type'] === \App\Models\Questions::RADIO)
                    <input type="radio" class="radio-question" id="{{$pageThreeQuestion['id']}}"
                           name="answer[{{$pageThreeQuestion['id']}}][answer]" value="disagree" required>
                    <label for="age1">مخالفم</label><br>
                    <input type="radio" class="radio-question" id="{{$pageThreeQuestion['id']}}"
                           name="answer[{{$pageThreeQuestion['id']}}][answer]" value="agree" required>
                    <label for="age2">موافقم</label><br>
                    <div class="questions-descriptions description_{{$pageThreeQuestion['id']}}" required>
                        <textarea id="answer" class="form-control" style="width: 100%; height: 100px"
                                  name="answer[{{$pageThreeQuestion['id']}}][description]"></textarea>
                    </div>
                @elseif ($pageThreeQuestion['type'] === \App\Models\Questions::PROGRESS)
                    @foreach (json_decode($pageThreeQuestion['options'], true) as $answer)
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="range" class="form-control-range"
                                       name="answer[{{$pageThreeQuestion['id']}}][answer][]"
                                       min="0" max="10" id="{{$pageThreeQuestion['id']}}"
                                       value="0"
                                       oninput="this.nextElementSibling.value = this.value"
                                >
                                <output>0</output>
                            </div>
                            <div class="col-sm-6">
                                <input type="checkbox" class="radio-question" id="{{$pageThreeQuestion['id']}}"
                                       name="answer[{{$pageThreeQuestion['id']}}][description][]" value="{{$answer}}">
                                <label for="age2">{{$answer}}</label><br>
                            </div>
                        </div>
                    @endforeach
                @elseif ($pageThreeQuestion['type'] === \App\Models\Questions::DESCRIPTION)
                    <textarea id="{{$pageThreeQuestion['id']}}" class="form-control" style="width: 100%; height: 100px"
                              name="answer[{{$pageThreeQuestion['id']}}][answer]" required></textarea>
                @elseif ($pageThreeQuestion['type'] === \App\Models\Questions::MULTIPLE_CHOICE)
                    @foreach (json_decode($pageThreeQuestion['options'], true) as $answer)
                        <input type="radio" id="{{$pageThreeQuestion['id']}}"
                               name="answer[{{$pageThreeQuestion['id']}}][answer]"
                               value="{{$answer}}" required>
                        <label for="{{$pageThreeQuestion['id']}}">{{$answer}}</label><br>
                    @endforeach
                @endif
                <hr>
            @endforeach
        </div>
        <div class="submit-form col-sm-6">
            <button type="submit" class="btn btn-success">ثبت</button>
        </div>
        <div class="submit-form col-sm-6">
            <button type="button" class="btn btn-success previous-third-step">صفحه قبل</button>
        </div>
    </form>
</div>
</body>
</html>

<script>
    $(document).ready(function () {
        $('.pageTwo').hide();
        $('.pageThree').hide();
        $('.questions-descriptions').hide();
        $('.submit-form').hide();
        $(".second-step").click(function () {
            $('.pageOne').hide();
            $('.pageTwo').show();
        });

        $(".previous-second-step").click(function () {
            $('.pageOne').show();
            $('.pageTwo').hide();
        });

        $(".third-step").click(function () {
            $('.pageTwo').hide();
            $('.pageThree').show();
            $('.submit-form').show();
        });

        $(".previous-third-step").click(function () {
            $('.pageOne').hide();
            $('.pageThree').hide();
            $('.pageTwo').show();
            $('.submit-form').hide();
        });

        $(".radio-question").change(function () {
            if ($(this).val() === 'disagree') {
                $('.description_' + $(this).attr('id')).show(1000);
            } else {
                $('.description_' + $(this).attr('id')).hide(1000);
            }
        });
    });
</script>
