<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Questions extends Authenticatable
{
    use HasFactory;

    public const RADIO = 'Radio';
    public const PROGRESS = 'Progress';
    public const DESCRIPTION = 'Descriptive';
    public const MULTIPLE_CHOICE = 'Multiple choice';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'type',
        'page',
        'options'
    ];

    public function setOptionsAttribute($value)
    {
        $collection = collect($value);
        $value = $collection->filter(function ($value, $key) {
            return $value != null;
        })->toArray();

        if ($this->attributes['type'] !== 'Multiple choice') {
            $this->attributes['options'] = '';
        }

        $this->attributes['options'] = json_encode($value);
    }
}
