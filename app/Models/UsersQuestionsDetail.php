<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UsersQuestionsDetail extends Authenticatable
{
    use HasFactory;

//    public $table = '';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_question_id',
        'question_id',
        'answer',
        'description'
    ];

    public function userQuestion()
    {
        return $this->hasOne(UsersQuestions::class, 'id', 'user_question_id');
    }

    public function question()
    {
        return $this->hasOne(Questions::class, 'id', 'question_id');
    }
}
