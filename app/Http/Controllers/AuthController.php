<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AuthController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function showLogin()
    {
        return View::make('login');
    }

    /**
     * @param LoginRequest $request
     * @return RedirectResponse
     */
    public function login(LoginRequest $request)
    {
        $credential = ['username' => $request->username, 'password' => $request->password];
        if (Auth::attempt($credential)) {
            return Redirect::to('dashboard');
        }

        return Redirect::to('login');
    }

    /**
     * @return RedirectResponse
     */
    public function logout()
    {
        Auth::logout();

        return Redirect::to('login');
    }
}
