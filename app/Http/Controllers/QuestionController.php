<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\StoreQuestionRequest;
use App\Models\Questions;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class QuestionController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $questions = Questions::all()->toArray();

        return View::make('question-list', ['questions' => $questions]);
    }

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return View::make('question-create');
    }

    /**
     * @param StoreQuestionRequest $questionRequest
     * @return RedirectResponse
     */
    public function store(StoreQuestionRequest $questionRequest)
    {
        DB::beginTransaction();
        try {
            $data = $questionRequest->all();
            $question = new Questions();
            $question->fill($data);
            $question->save();
            DB::commit();

            return Redirect::to('questions/list');
        } catch (\Exception $exception) {
            DB::rollBack();
            dd($exception->getMessage());

            return Redirect::to('questions/create');
        }
    }
}
