<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\View|RedirectResponse
     */
    public function __invoke()
    {
        if (!Auth::check()) {
            return Redirect::to('login');
        }

        return View::make('dashboard');
    }
}
