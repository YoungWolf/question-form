<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserQuestionRequest;
use App\Models\Questions;
use App\Models\UsersQuestions;
use App\Models\UsersQuestionsDetail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class UsersQuestionController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $pageOneQuestions = Questions::where('page', '=', 1)->get()->toArray();
        $pageTwoQuestions = Questions::where('page', '=', 2)->get()->toArray();
        $pageThreeQuestions = Questions::where('page', '=', 3)->get()->toArray();

        return View::make('users-questions',
            [
                'pageOneQuestions' => $pageOneQuestions,
                'pageTwoQuestions' => $pageTwoQuestions,
                'pageThreeQuestions' => $pageThreeQuestions,
            ]
        );
    }

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function list()
    {
        $usersQuestions = UsersQuestions::all();

        return View::make('users-questions-list', ['usersQuestions' => $usersQuestions]);
    }

    /**
     * @param StoreUserQuestionRequest $request
     * @return RedirectResponse
     */
    public function store(StoreUserQuestionRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->all()['answer'] ?? [];
            $userQuestion = new UsersQuestions();
            $userQuestion->user_id = Auth::user()->id;
            $userQuestion->total_answer = count($data);
            $userQuestion->save();
            foreach ($data as $item) {
                $result = $item['description'] ?? null;
                $answer = $item['answer'] ?? null;
                if ($item['type'] === 'Progress') {
                    $result = [];
                    $counter = 0;
                    foreach ($item['answer'] as $itemData) {
                        if ($itemData !== '0') {
                            if (isset($item['description'][$counter])) {
                                $result[$itemData] = $item['description'][$counter];
                                $counter++;
                            }
                        }
                    }
                    $result = json_encode($result);
                    $answer = $result;
                }
                $userQuestionDetail = new UsersQuestionsDetail();
                $userQuestionDetail->question_id = $item['question_id'];
                $userQuestionDetail->answer = $answer;
                $userQuestionDetail->description = $result;
                $userQuestionDetail->user_question_id = $userQuestion->id;
                $userQuestionDetail->save();
            }
            DB::commit();

            return Redirect::to('/dashboard');
        } catch (\Exception $exception) {
            DB::rollBack();

            return Redirect::to('/users/questions');
        }
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\View
     */
    public function view(int $id)
    {
        $usersQuestionsDetails = UsersQuestionsDetail::with(['question', 'userQuestion'])
            ->where('user_question_id', '=', $id)
            ->get();

        return View::make('users-questions-view', ['usersQuestionsDetails' => $usersQuestionsDetails]);
    }
}
